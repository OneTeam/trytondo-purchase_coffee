# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

_CERTIFICATE = [
    ('cafe_practices', 'C.A.F.E Practices'),
    ('standard', 'Estándar'),
    ('rainforest', 'Rainforest'),
    (None, ''),
]

_digits = (16, 2) 

class Invoice(metaclass=PoolMeta):
    'Invoice'
    __name__ = 'account.invoice'

    advance = fields.Boolean('Advance')
    certificate = fields.Selection(_CERTIFICATE, 'Certificate')
    performance_rate = fields.Numeric('Performance Rate', _digits)
    price_kilo = fields.Numeric('Price Kilo', _digits)
    price_charge = fields.Numeric('Price Charge', _digits)
    
