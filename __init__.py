# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import purchase
from . import product
from . import configuration
def register():
    Pool.register(
        purchase.Purchase,
        purchase.PurchaseLine,
        purchase.PurchaseFixing,
        product.Template,
        configuration.Configuration,
        module='purchase', type_='model')
