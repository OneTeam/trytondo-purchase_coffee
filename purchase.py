# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.model import Model, ModelView, ModelSQL, \
    fields, sequence_ordered
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.pyson import Eval, Bool
from trytond.modules.product import round_price
from decimal import Decimal
from collections import namedtuple

_digits = (16, 2) 
_digits_package = (16, 0)
_sacks = Decimal('0.65')
_canvas = Decimal('0.2')
_tax_ecoosolida = 'Ecoosolida'

_STATES_READONLY = {
    'readonly': Eval('type') != 'None',
}

_STATES_REQUIRED_ALL = {
    'readonly': Bool(Eval('product')),
    'required': Eval('type') != 'None',
}

_TaxableLine = namedtuple(
    '_TaxableLine', ('taxes', 'unit_price', 'quantity', 'tax_date'))

class Purchase(metaclass=PoolMeta):
    'Purchase'
    __name__ = 'purchase.purchase'

    fixings = fields.One2Many('purchase.fixing', 'purchase', 'Fixings')
    tax_amount_ecoosolida = fields.Function(
        fields.Numeric('Tax Ecoosolida',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_amount')
    tax_amount_ecoosolida_cache = fields.Numeric('Tax Ecoosolida Cache',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    tax_amount_withholding = fields.Function(
        fields.Numeric('Tax Withholding',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_amount')
    tax_amount_withholding_cache = fields.Numeric('Tax Withholding Cache',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    tax_amount_bank_discounts = fields.Function(
        fields.Numeric('Tax Bank Discounts',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_amount')
    tax_amount_bank_discounts_cache = fields.Numeric('Tax Bank Discount Cache',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    tax_amount_others = fields.Function(
        fields.Numeric('Tax Others',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_amount')
    tax_amount_others_cache = fields.Numeric('Tax Others Cache',
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    total_pergamino = fields.Function(
        fields.Float('Total Pergamino',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_total_kilos_product')
    total_pasilla = fields.Function(
        fields.Float('Total Pasilla',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_total_kilos_product')
    total_corriente = fields.Function(
        fields.Float('Total Corriente',
                       digits=(16, Eval('currency_digits', 2)),
                       depends=['currency_digits']), 'get_total_kilos_product')

    @staticmethod
    def default_total_pergamino():
        return 0

    @staticmethod
    def default_total_corriente():
        return 0

    @staticmethod
    def default_total_pasilla():
        return 0
    
    @fields.depends('lines', 'currency',
                    methods=['get_tax_amount', 'get_tax_amount_ecoosolida'])    
    def on_change_lines(self):
        self.untaxed_amount = Decimal('0.0')
        self.tax_amount = Decimal('0.0')
        self.total_amount = Decimal('0.0')
        self.tax_amount_ecoosolida = Decimal('0.0')
        self.tax_amount_withholding = Decimal('0.0')
        self.tax_amount_bank_discounts = Decimal('0.0')
        self.tax_amount_others = Decimal('0.0')
        if self.lines:
            for line in self.lines:
                self.untaxed_amount += getattr(line, 'amount', None) or 0
            self.tax_amount = self.get_tax_amount()
            self.tax_amount_ecoosolida = self.get_tax_amount_ecoosolida()
            self.tax_amount_withholding = self.get_tax_amount_withholding()
            self.tax_amount_bank_discounts = self.get_tax_amount_bank_discounts()
            self.tax_amount_others = self.get_tax_amount_others()
        if self.currency:
            self.untaxed_amount = self.currency.round(self.untaxed_amount)
            self.tax_amount = self.currency.round(self.tax_amount)
        self.total_amount = self.untaxed_amount + self.tax_amount
        if self.currency:
            self.total_amount = self.currency.round(self.total_amount)
    
    @fields.depends(methods=['_get_tax_context', '_round_taxes'])
    def _get_taxes(self):
        pool = Pool()
        Tax = pool.get('account.tax')
        Configuration = pool.get('account.configuration')
        Purchase_Configuration = pool.get('purchase.configuration')

        taxes = {}
        with Transaction().set_context(self._get_tax_context()):
            config = Configuration(1)
            purchase_config = Purchase_Configuration(1)
            
            tax_rounding = config.get_multivalue('tax_rounding')
            taxable_lines = [_TaxableLine(*params)
                for params in self.taxable_lines]
            for line in taxable_lines:
                l_taxes = Tax.compute(Tax.browse(line.taxes), line.unit_price,
                    line.quantity, line.tax_date or self.tax_date)
                for tax in l_taxes:
                    taxline = self._compute_tax_line(**tax)
                    # Base must always be rounded per line as there will be one
                    # tax line per taxable_lines
                    taxline['amount_ecoosolida'] = 0
                    taxline['amount_withholding'] = 0
                    taxline['amount_bank_discounts'] = 0
                    taxline['amount_others'] = 0
                    if self.currency:
                        taxline['base'] = self.currency.round(taxline['base'])
                        taxline['amount'] = self.currency.round(taxline['amount'])
                    if taxline not in taxes:
                        taxes[taxline] = taxline
                    else:
                        taxes[taxline]['base'] += taxline['base']
                        taxes[taxline]['amount'] += taxline['amount']
                    if tax['tax'].group == purchase_config.tax_group_ecoosolida:
                        taxes[taxline]['amount_ecoosolida'] += taxline['amount']
                    elif tax['tax'].group == purchase_config.tax_group_withholding:
                        taxes[taxline]['amount_withholding'] += taxline['amount']
                    elif tax['tax'].group == purchase_config.tax_group_bank_discounts:
                        taxes[taxline]['amount_bank_discounts'] += taxline['amount']
                    else:
                        taxes[taxline]['amount_others'] += taxline['amount']
                if tax_rounding == 'line':
                    self._round_taxes(taxes)
        if tax_rounding == 'document':
            self._round_taxes(taxes)
        return taxes

    @fields.depends(methods=['_get_taxes'])
    def get_tax_amount(self):
        taxes = iter(self._get_taxes().values())
        return sum((tax['amount'] for tax in taxes), Decimal(0))
    
    @fields.depends(methods=['_get_taxes'])
    def get_tax_amount_ecoosolida(self):
        taxes = iter(self._get_taxes().values())
        return sum((tax['amount_ecoosolida'] for tax in taxes), Decimal(0))
        
    @fields.depends(methods=['_get_taxes'])
    def get_tax_amount_withholding(self):
        taxes = iter(self._get_taxes().values())
        return sum((tax['amount_withholding'] for tax in taxes), Decimal(0))

    @fields.depends(methods=['_get_taxes'])
    def get_tax_amount_bank_discounts(self):
        taxes = iter(self._get_taxes().values())
        return sum((tax['amount_bank_discounts'] for tax in taxes), Decimal(0))    
    
    @fields.depends(methods=['_get_taxes'])
    def get_tax_amount_others(self):
        taxes = iter(self._get_taxes().values())
        return sum((tax['amount_others'] for tax in taxes), Decimal(0))
        
    @classmethod
    def get_amount(cls, purchases, names):
        untaxed_amount = {}
        tax_amount = {}
        total_amount = {}
        tax_amount_ecoosolida = {}
        tax_amount_withholding = {}
        tax_amount_bank_discounts = {}
        tax_amount_others = {}
        
        if {'tax_amount', 'total_amount'} & set(names):
            compute_taxes = True
        else:
            compute_taxes = False
        # Sort cached first and re-instanciate to optimize cache management
        purchases = sorted(purchases,
            key=lambda p: p.state in cls._states_cached, reverse=True)
        purchases = cls.browse(purchases)
        for purchase in purchases:
            if (purchase.state in cls._states_cached
                    and purchase.untaxed_amount_cache is not None
                    and purchase.tax_amount_cache is not None
                    and purchase.total_amount_cache is not None):
                untaxed_amount[purchase.id] = purchase.untaxed_amount_cache
                if compute_taxes:
                    tax_amount_ecoosolida[purchase.id] = purchase.tax_amount_ecoosolida_cache
                    tax_amount_withholding[purchase.id] = purchase.tax_amount_withholding_cache
                    tax_amount_bank_discounts[purchase.id] = purchase.tax_amount_bank_discounts_cache
                    tax_amount_others[purchase.id] = purchase.tax_amount_others_cache
                    tax_amount[purchase.id] = purchase.tax_amount_cache
                    total_amount[purchase.id] = purchase.total_amount_cache
            else:
                untaxed_amount[purchase.id] = sum(
                    (line.amount for line in purchase.lines
                        if line.type == 'line'), Decimal(0))
                if compute_taxes:
                    tax_amount_ecoosolida[purchase.id] = purchase.get_tax_amount_ecoosolida()
                    tax_amount_withholding[purchase.id] = purchase.get_tax_amount_withholding()
                    tax_amount_bank_discounts[purchase.id] = purchase.get_tax_amount_bank_discounts()
                    tax_amount_others[purchase.id] = purchase.get_tax_amount_others()
                    tax_amount[purchase.id] = purchase.get_tax_amount()
                    total_amount[purchase.id] = (
                        untaxed_amount[purchase.id] + tax_amount[purchase.id])

        result = {
            'untaxed_amount': untaxed_amount,
            'tax_amount_ecoosolida': tax_amount_ecoosolida,
            'tax_amount_withholding': tax_amount_withholding,
            'tax_amount_bank_discounts': tax_amount_bank_discounts,
            'tax_amount_others': tax_amount_others,
            'tax_amount': tax_amount,
            'total_amount': total_amount,
            }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    @classmethod
    def get_total_kilos_product(cls, purchases, names):
        total_pergamino = {}
        total_corriente = {}
        total_pasilla = {}

        # Sort cached first and re-instanciate to optimize cache management
        purchases = sorted(purchases,
            key=lambda p: p.state in cls._states_cached, reverse=True)
        purchases = cls.browse(purchases)
        for purchase in purchases:
            total_pergamino[purchase.id] = float(0.0)
            total_corriente[purchase.id] = float(0.0)
            total_pasilla[purchase.id] = float(0.0)
            if purchase.lines:
                for line in purchase.lines:
                    if line.product.type_coffee == 'pergamino':
                        total_pergamino[purchase.id] += line.quantity
                    elif line.product.type_coffee == 'corriente':
                        total_corriente[purchase.id] += line.quantity
                    elif line.product.type_coffee == 'pasilla':
                        total_pasilla[purchase.id] += line.quantity
        result = {
            'total_pergamino': total_pergamino,
            'total_corriente': total_corriente,
            'total_pasilla': total_pasilla,
            }
        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result
    
    @classmethod
    def store_cache(cls, purchases):
        for purchase in purchases:
            purchase.untaxed_amount_cache = purchase.untaxed_amount
            purchase.tax_amount_cache = purchase.tax_amount
            purchase.total_amount_cache = purchase.total_amount
            purchase.tax_amount_ecoosolida_cache = round(purchase.tax_amount_ecoosolida, 2)
            purchase.tax_amount_withholding_cache = round(purchase.tax_amount_withholding, 2)
            purchase.tax_amount_bank_discounts_cache = round(purchase.tax_amount_bank_discounts, 2)
            purchase.tax_amount_others_cache = round(purchase.tax_amount_others, 2)
        cls.save(purchases)
    
    
class PurchaseLine(metaclass=PoolMeta):
    'Purchase Line'
    __name__ = 'purchase.line'

    fixing = fields.Many2One('purchase.fixing', 'Fixing',
                             states= { 'readonly': Bool(~Eval('performance_rate')) |
                                       Bool(Eval('product')),
                                       'required': Eval('type') != 'None'},
                             domain=[('purchase.id', '=', Eval('_parent_purchase.id'))],
                             depends=['purchase', 'bonus_total'])
    package_sacks = fields.Numeric('Package Sacks', _digits_package,
                                   states=_STATES_REQUIRED_ALL,
                                   domain=[('package_sacks', '>=', 0)],
                                   depends=['product', 'type'])
    package_canvas = fields.Numeric('Package Canvas', _digits_package,
                                    states={ 'readonly': Bool(Eval('product')),
                                             'required': Bool(Eval('type'))},
                                   domain=[('package_canvas', '>=', 0)],
                                    depends=['product', 'type', 'package_sacks'])
    package_numbers = fields.Numeric('Package Numbers', _digits_package,
                                     states= _STATES_READONLY,
                                     depends=['type'])
    gross_weight = fields.Numeric('Gross Weight', _digits,
                                     states={ 'readonly': Bool(Eval('product')) |
                                              Bool(~Eval('package_numbers')),
                                              'required': Bool(Eval('type')) 
                                             },
                                     depends=['product', 'type', 'package_numbers'])
    uncover = fields.Numeric('Uncover', _digits,
                             states=_STATES_READONLY,
                             depends=['type'])
    net_weight = fields.Numeric('Net Weight', _digits,
                                states=_STATES_READONLY,
                                depends=['type'])
    price_base = fields.Numeric('Price Base', _digits,
                                states=_STATES_READONLY,
                                depends=['type'])
    performance_rate = fields.Numeric('Performance Rate', _digits,
                                     states={ 'readonly': Bool(Eval('product')) |
                                              Bool(~Eval('gross_weight')),
                                              'required': Bool(Eval('type')) 
                                             },
                                     depends=['product', 'type', 'gross_weight'])
    bonus_total = fields.Numeric('Bonus Total', _digits,
                                 states=_STATES_READONLY,
                                 depends=['type'])
    price_estimated =  fields.Numeric('Price Estimated', _digits,
                                states=_STATES_READONLY,
                                depends=['type'])

        
    @fields.depends('gross_weight', 'uncover', 'net_weight',
                    'package_numbers', 'package_sacks', 'package_canvas')
    def on_change_gross_weight(self):
        if not self.gross_weight:
            self.get_recalculate_weight(self)
        else:
            package_quantity = 0
            sacks_weight = 0
            if self.package_sacks > 0:
                package_quantity = self.package_sacks
                sacks_weight = _sacks * self.package_sacks
            if self.package_canvas > 0:
                package_quantity += self.package_canvas
                sacks_weight += _canvas * self.package_canvas
            self.package_numbers = package_quantity
            self.uncover = sacks_weight
            self.net_weight = self.gross_weight - self.uncover
            self.quantity = float(self.net_weight)
        
    @fields.depends('price_base', 'fixing', 'product', 'quantity', 'taxes',
                    'unit', 'unit_price', 'bonus_total', 'performance_rate')
    def on_change_fixing(self):
        if not self.fixing:
            self.product = None
            self.unit_price = None
            self.unit = None
            self.price_base = None
            self.taxes = None
            self.price_estimated = None
        else:
            if self.fixing.product.template.type_coffee == 'pasilla':
                self.price_base = self.fixing.amount
                unit_price = (self.performance_rate * self.fixing.amount)/Decimal('12.5')
            else:
                self.price_base = self.fixing.amount/125
                unit_price = Decimal(self.fixing.amount/125*(1+(self.bonus_total/100)))
            self.unit = self.fixing.product.template.default_uom
            self.unit_price = unit_price
            self.price_estimated = unit_price
            self.product = self.fixing.product
            self.amount = Decimal(self.quantity) * self.unit_price
            if self.product.template.account_category.supplier_taxes:
                tax = self.product.template.account_category.supplier_taxes
                self.taxes = tax 
   
    @fields.depends('package_sacks', 'package_canvas', 'gross_weight',
                    'package_numbers', 'uncover', 'net_weight', 'performance_rate',
                    'bonus_total',
                    methods=['get_recalculate_weight', 'get_values_none'])
    def on_change_package_sacks(self):
        if self.package_sacks is None:
            self.get_recalculate_weight(self)

    @fields.depends('package_sacks', 'package_canvas', 'gross_weight',
                    'package_numbers', 'uncover', 'net_weight', 'performance_rate',
                    'bonus_total',
                    methods=['get_recalculate_weight', 'get_values_none'])
    def on_change_package_canvas(self):
        if self.package_canvas is None or \
           self.package_sacks is None or \
           (self.package_sacks + self.package_canvas) <= 0:
            self.get_recalculate_weight(self)
        else:
            self.package_numbers = self.package_canvas + self.package_sacks

    @fields.depends('package_sacks', 'package_canvas', 'gross_weight',
                    'package_numbers', 'uncover', 'net_weight', 'performance_rate',
                    'bonus_total',
                    methods=['get_recalculate_weight', 'get_values_none'])
    def on_change_performance_rate(self):
        if not self.performance_rate:
            self.get_recalculate_weight(self)
        else:
            self.bonus_total = 94 - self.performance_rate
        
    @fields.depends('product', 'quantity', 'fixing', 'bonus_total',
        methods=['_get_context_purchase_price'])
    def compute_unit_price(self):
        pool = Pool()
        Product = pool.get('product.product')

        if not self.product:
            return

        with Transaction().set_context(self._get_context_purchase_price()):
            unit_price = Product.get_purchase_price([self.product],
                abs(self.quantity or 0))[self.product.id]
            if unit_price:
                unit_price = round_price(unit_price)
            return Decimal(self.fixing.amount/125*(1+(self.bonus_total/100)))
    
    def get_recalculate_weight(self, name):
        if not self.package_sacks:
            self.get_values_none(self)
        elif not self.package_canvas:
            self.get_values_none(self)
        elif not self.package_numbers:
            self.get_values_none(self)
        elif not self.gross_weight:
            self.get_values_none(self)
        elif not self.performance_rate:
            self.get_values_none(self)
        else:
            return

    def get_values_none(self, name):
        self.package_sacks = None
        self.package_canvas = None
        self.package_numbers = None
        self.gross_weight = None
        self.uncover = None
        self.net_weight = None
        self.performance_rate = None
        self.bonus_total = None
        

class PurchaseFixing(sequence_ordered(), ModelView, ModelSQL):
    'Purchase Fixing'
    __name__ = 'purchase.fixing'

    purchase = fields.Many2One('purchase.purchase', 'Purchase',
                               ondelete='CASCADE')
    name = fields.Char('Name')
    amount = fields.Numeric('Amount')
    product = fields.Many2One('product.product', 'Product')
