# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

class Configuration(metaclass=PoolMeta):
    'Purchase Configuration'
    __name__ = 'purchase.configuration'

    tax_group_withholding = fields.Many2One('account.tax.group', 'Withholding')
    tax_group_ecoosolida = fields.Many2One('account.tax.group', 'Ecoosolida')
    tax_group_bank_discounts = fields.Many2One('account.tax.group', 'Bank Discounts')
