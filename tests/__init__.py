try:
    from trytond.modules.purchase_coffee.tests.test_purchase_coffee import suite  # noqa: E501
except ImportError:
    from .test_purchase_coffee import suite

__all__ = ['suite']
