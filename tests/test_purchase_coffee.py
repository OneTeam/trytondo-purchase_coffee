import unittest


from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class PurchaseCoffeeTestCase(ModuleTestCase):
    'Test Purchase Coffee module'
    module = 'purchase_coffee'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PurchaseCoffeeTestCase))
    return suite
