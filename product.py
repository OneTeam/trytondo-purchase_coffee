# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

_TYPE_COFFEE = [
    ('pergamino', 'Pergamino'),
    ('corriente', 'Corriente'),
    ('pasilla', 'Pasilla'),
]

class Template(metaclass=PoolMeta):
    'Product'
    __name__ = 'product.template'

    type_coffee = fields.Selection(_TYPE_COFFEE, 'Type Coffee')
